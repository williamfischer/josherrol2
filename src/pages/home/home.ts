import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';

import { ContactPage } from '../contact/contact';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  displayName: any;
  loggedin: boolean;

  constructor(public navCtrl: NavController, public afAuth: AngularFireAuth) {

    afAuth.authState.subscribe(user => {
      if (!user) {
        this.displayName = null;
        this.loggedin = false;
        return;
      }
      this.displayName = user.displayName;

      if(this.displayName){
        this.navCtrl.push(ContactPage);
        this.loggedin = true;
      }


      console.log(user.displayName)
    });


  }

  loginFB() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider());
    this.navCtrl.push(ContactPage);
  }

  logout() {
    this.afAuth.auth.signOut();
  }


  loginG() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
    this.navCtrl.push(ContactPage);
  }

}
