import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import * as firebase from 'firebase';

import { AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { AboutPage } from '../about/about';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})

export class ContactPage {
   itemsRef: AngularFireList<any>;
   roomsRef: AngularFireList<any>;
   inList: boolean;
   displayName: string;
   roomlength: string;
   searching: boolean;

   constructor(public navCtrl: NavController, public afAuth: AngularFireAuth, public db: AngularFireDatabase, public alertCtrl: AlertController) {
      this.itemsRef = db.list('users');
      this.roomsRef = db.list('rooms');

      this.inList = false;

      afAuth.authState.subscribe(user => {
         //if isnt signed in push back to main page
         if (!user) {
            this.displayName = null;
            this.navCtrl.push(HomePage);
            return;
         }
         this.displayName = user.displayName;
       });
   }//end constructor

   createUser(){
      this.searching = true;
      var searchStack = [];

      this.itemsRef.set(this.afAuth.auth.currentUser.uid, {
         username: this.afAuth.auth.currentUser.displayName,
         UID: this.afAuth.auth.currentUser.uid,
         isSearching: true,
         chattingTo: false
      });
      //console.log(this.itemsRef.child(this.afAuth.auth.currentUser.uid));

      if(this.searching){
         this.itemsRef.valueChanges().subscribe(action => {
            action.forEach(item => {
               if(item.isSearching == true && item.UID !== this.afAuth.auth.currentUser.uid && !item.chattingTo){
                  searchStack.push({
                     UID: item.UID,
                     Username: item.username
                 })
               }
            });//end foreach

            if(searchStack.length == 0){
               console.log("NO MATCHES FOUND");
            } else {//people in stack
               this.searching = false;
               console.log("YOU HAVE MATCHED WITH " + searchStack[0].Username);

               this.itemsRef.update(this.afAuth.auth.currentUser.uid, {
                  isSearching: false,
                  chattingTo: searchStack[0].UID
               });
               this.itemsRef.update(searchStack[0].UID, {
                  isSearching: false,
                  chattingTo: this.afAuth.auth.currentUser.uid
               });

               this.roomsRef.valueChanges().subscribe(action => {
                  this.roomsRef.set(this.afAuth.auth.currentUser.uid,{
                     user1: this.afAuth.auth.currentUser.uid,
                     user2: searchStack[0].UID
                  });
                  this.navCtrl.push(AboutPage);
               });

            }//end else
         });//end subscribe itemsref
      }//end if searching
   }//end createUser

   logout() {
      this.afAuth.auth.signOut();
      this.navCtrl.push(HomePage);
      this.searching = false;
   }

   stopSearch(){
       this.itemsRef.update(this.afAuth.auth.currentUser.uid, {
         isSearching: false,
         chattingTo: false
       });
       let alert2 = this.alertCtrl.create({
        title: 'You have left the queue',
        subTitle: 'Please try again soon',
        buttons: ['Dismiss']
       });
       alert2.present();
       this.searching = false;
   }
}//end export class ContactPage
